- Add parsing of module names to `skyd -M` and automatically enable the
    `accounting`and `feemanager` modules if the `wallet` is enabled.
